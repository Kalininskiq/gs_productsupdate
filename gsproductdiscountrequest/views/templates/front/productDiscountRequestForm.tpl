/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

{extends file=$layout}
{block name='content'}
<h1>Request for a product discount</h1>
<form id="discountRequestForm" method="POST">
    <div class="form-group">
        <label for="userName">Name</label>
        <input type="text" name="user_name" id="userName" class="form-control"  placeholder="Username">
    </div>

    <div class="form-group">
        <label for="userEmail">Email</label>
        <input type="email" name="email" id="userEmail" class="form-control"  placeholder="Enter email">
    </div>

    <div class="form-group">
        <label for="productsForDiscount">Select a product</label>
        <select id="productsForDiscount" required multiple class="form-control">
            {foreach $productsForSelect as $product}
                <option value="{$product.id_product|escape:'htmlall':'UTF-8'}">{$product.name|escape:'htmlall':'UTF-8'}</option>
            {/foreach}
        </select>
    </div>

    <div class="form-group">
         <textarea name="message" id="message" class="form-control">

    </textarea>
    </div>

    <div class="form-check">
        <input type="checkbox" id="termsAndConditions" name="terms" required>
        <label class="form-check-label" for="termsAndConditions">Do you agree to the General Terms and Conditions</label>
    </div>

    <button type="submit" class="btn btn-primary" id="DiscountFormButton">Submit</button>
</form>
    <br>
    <div id="errors">

    </div>

{/block}
