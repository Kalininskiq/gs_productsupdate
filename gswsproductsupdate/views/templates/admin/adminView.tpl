{*
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<div class="panel col-md-4" >
    <h3><i class="icon icon-tags"></i>Category Mapper</h3>
    <form id="categoryMap" method="POST">
        <div class="form-group" >
            <label for="local_category">Select a local category to map</label>
            <select id="local_category" required  class="form-control" name="local_category">
                {foreach $localCategories as $localCategory}
                    <option value="{$localCategory.id_category|escape:'htmlall':'UTF-8'}">{$localCategory.name|escape:'htmlall':'UTF-8'}</option>
                {/foreach}
            </select>
        </div>
        <div class="form-group" >
            <label for="remote_category">Select a remote category to map</label>
            <select id="remote_category" required  class="form-control" name="remote_category">
                {foreach $remoteCategories as $remoteCategory}
                    <option value="{$remoteCategory.id|escape:'htmlall':'UTF-8'}">{$remoteCategory.name['language']|escape:'htmlall':'UTF-8'}</option>
                {/foreach}
            </select>
        </div>

        <div class="form-group" >
            <label for="local_manufacturer">Select a local manufacturer to map</label>
            <select id="local_category" required  class="form-control" name="local_manufacturer">
                {foreach $localManufacturers as $localManufacturer}
                    <option value="{$localManufacturer.id_manufacturer|escape:'htmlall':'UTF-8'}">{$localManufacturer.name|escape:'htmlall':'UTF-8'}</option>
                {/foreach}
            </select>
        </div>
        <div class="form-group" >
            <label for="remote_manufacturer">Select a remote manufacturer to map to</label>

            <select id="remote_manufacturer" required  class="form-control" name="remote_manufacturer">
                {foreach $remote_manufacturers as $remote_manufacturer}
                    <option value="{$remote_manufacturer.id|escape:'htmlall':'UTF-8'}">{$remote_manufacturer.name|escape:'htmlall':'UTF-8'}</option>
                {/foreach}
            </select>
        </div>
        <button type="submit" class="btn btn-primary" id="MapCategoryButton" name="mapCategoryButton">Submit</button>
    </form>
</div>