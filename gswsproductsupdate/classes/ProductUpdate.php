<?php


class ProductUpdate extends ObjectModel
{
    public $id_object;
    public $id_object_remote;
    public $type;


    public static $definition = array(
        'table' => 'gswsproductsupdate',
        'primary' => 'id_gswsproductsupdate',
        'fields' => array(
            'id_object_remote' => array('type' => self::TYPE_INT, 'validate' => 'isInt', 'required' => true),
            'id_object' => array('type' => self::TYPE_INT, 'validate' => 'isInt', 'required' => true),
            'type' => array('type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => true),
        )
    );

    public static function checkObject($id_object_remote,$type = null)
    {
        return (bool)Db::getInstance()->getValue(
            'SELECT COUNT(*) FROM `'._DB_PREFIX_.'gswsproductsupdate` WHERE `id_object_remote` = '.(int)$id_object_remote .' AND `type` = "'.$type.'"'
        );
    }


    public static function checkProduct($id_product_remote)
    {
        return (bool)Db::getInstance()->getValue();
    }

}