<?php


class CategoryMapper extends ObjectModel
{
    public $id_category_local;
    public $id_category_remote;
    public $id_manufacturer_remote;
    public $id_manufacturer_local;
    public static $definition = array(
        'table' => 'gsws_mapped_categories',
        'primary' => 'id_gsws_mapped_categories',
        'fields' => array(
        'id_category_local' => array('type' => self::TYPE_INT, 'validate' => 'isInt', 'required' => true),
        'id_category_remote' => array('type' => self::TYPE_INT, 'validate' => 'isInt', 'required' => true),
            'id_manufacturer_local' => array('type' => self::TYPE_INT, 'validate' => 'isInt', 'required' => true),
            'id_manufacturer_remote' => array('type' => self::TYPE_INT, 'validate' => 'isInt', 'required' => true),
        ));


    public static function  checkCategory($id_category_remote,$type = null)
    {
        return (bool)Db::getInstance()->getValue(
            'SELECT COUNT(*) FROM `'._DB_PREFIX_.'gswsproductsupdate` WHERE `$id_category_remote` = '.(int)$id_category_remote .' AND `type` = "'.$type.'"'
        );
    }


    public static function  checkManufacturer($id_manufacturer_remote,$type = null)
    {
        return (bool)Db::getInstance()->getValue(
            'SELECT COUNT(*) FROM `'._DB_PREFIX_.'gswsproductsupdate` WHERE `$id_manufacturer_remote` = '.(int)$id_manufacturer_remote .' AND `type` = "'.$type.'"'
        );
    }
}