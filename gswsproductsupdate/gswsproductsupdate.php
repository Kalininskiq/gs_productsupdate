<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
include_once(dirname(__FILE__).'./classes/ProductUpdate.php');
include_once(dirname(__FILE__).'./classes/CategoryMapper.php');
if (!defined('_PS_VERSION_')) {
    exit;
}

class Gswsproductsupdate extends Module
{
    protected $config_form = false;
    protected $url;
    protected $key;
    protected $debug;
    protected $webService = null;
    protected $accessToken;
    public function __construct()
    {
        $this->name = 'gswsproductsupdate';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'Kalin Ivanov';
        $this->need_instance = 1;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('GS_ProductsUpdate');
        $this->description = $this->l('Updating existing products');
        $this->controllers = array('mapCategories');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall the module?');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);

        $this->accessToken = md5(_COOKIE_KEY_.Configuration::get('PS_SHOP_NAME'));
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('GSWSPRODUCTSUPDATE_LIVE_MODE', false);


        include(dirname(__FILE__).'/sql/install.php');

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader')&&
            $this->installTab();
    }

    public function uninstall()
    {
        Configuration::deleteByName('GSWSPRODUCTSUPDATE_LIVE_MODE');
        Configuration::deleteByName('GSWSPRODUCTSUPDATE_KEY');
        Configuration::deleteByName('GSWSPRODUCTSUPDATE_DATA_FORMAT');
        Configuration::deleteByName('GSWSPRODUCTSUPDATE_URL');


        include(dirname(__FILE__).'/sql/uninstall.php');

        return parent::uninstall()
            && $this->uninstallTab();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {

        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitGswsproductsupdateModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);
        $urlParams = [
            'token' => $this->accessToken
        ];
        $updateUrl = $this->context->link->getModuleLink($this->name,'update').'?'.http_build_query($urlParams);
        $this->context->smarty->assign(['url' => $updateUrl]);
        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitGswsproductsupdateModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Debug mode'),
                        'name' => 'GSWSPRODUCTSUPDATE_DEBUG_MODE',
                        'is_bool' => true,
                        'desc' => $this->l('Use this module in debug mode'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'radio',
                        'label' => $this->l('Data format'),
                        'name' => 'GSWSPRODUCTSUPDATE_DATA_FORMAT',
                        'is_bool' => true,
                        'desc' => $this->l('Format the output data from the API'),
                        'values' => array(
                            array(
                                'id' => 'xml',
                                'value' => 'xml',
                                'label' => $this->l('XML')
                            ),
                            array(
                                'id' => 'json',
                                'value' => 'json',
                                'label' => $this->l('JSON')
                            )
                        ),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('Enter a valid url'),
                        'name' => 'GSWSPRODUCTSUPDATE_URL',
                        'label' => $this->l('URL'),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'desc' => $this->l('Enter a valid key'),
                        'name' => 'GSWSPRODUCTSUPDATE_KEY',
                        'label' => $this->l('Key'),
                    ),

                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );


    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'GSWSPRODUCTSUPDATE_DEBUG_MODE' =>
                Tools::getValue('GSWSPRODUCTSUPDATE_DEBUG_MODE'),
            'GSWSPRODUCTSUPDATE_URL' =>
                Tools::getValue('GSWSPRODUCTSUPDATE_URL'),
            'GSWSPRODUCTSUPDATE_KEY' =>
                Tools::getValue('GSWSPRODUCTSUPDATE_KEY'),
            'GSWSPRODUCTSUPDATE_DATA_FORMAT' =>
                Tools::getValue("GSWSPRODUCTSUPDATE_DATA_FORMAT"),
        );

    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        $errors = [];
        if(!Validate::isUrl(Tools::getValue("GSWSPRODUCTSUPDATE_URL")) || empty(Tools::getValue("GSWSPRODUCTSUPDATE_URL")))
        {
            $errors[] = "Please enter a valid URL";
        }
        if (!empty($errors)){
            return $this->displayError($errors);
        }
        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {

        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }
    
    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        Media::addJsDef([
            'UrlToAdminController' => Context::getContext()->link->
            getModuleLink('gswsproductsupdate', 'AdminMapCategoriesController', array('ajax'=>true))
        ]);
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    protected function _config()
    {
        $this->url = Configuration::get("GSWSPRODUCTSUPDATE_URL");
        $this->key = Configuration::get("GSWSPRODUCTSUPDATE_KEY");
        $this->debug = Configuration::get("GSWSPRODUCTSUPDATE_DEBUG_MODE");
    }


     protected function _init()
    {
        if ($this->webService !== null)
            return false;
        try {

            include_once(dirname(__FILE__).'./PSWebServiceLibrary.php');
            $this->_config();
            $this->webService = new PrestaShopWebservice($this->url,$this->key,false);
        } catch (PrestaShopWebserviceException $e) {
            $e->getMessage();
        }
    }

    public function getProducts($id_product = null)
    {
        $this->_init();
        $opt = [
            'resource' => 'products',
            'language' => "1"

        ];
        if ($id_product !== null)
            $opt['id'] = $id_product;
        return $this->webService->get($opt);

    }

    public function createProducts()
    {

        $xml = $this->getProducts();
        $states = [];

        foreach($xml->children()->children() as $state)
        {
            $states[] = json_decode(json_encode($this->getProducts($state->attributes()->id)->children()->children()),true);
        }
//          $product = json_decode(json_encode($xml->product),true);

        foreach ($states as $product)
        {
//            if (ProductUpdate::checkObject($product['id'],'product')){
//                //update
//                return false;
//            } else {
                //create
                $productObj = new Product();
               
                $languages = Language::getLanguages();
                foreach ($languages as $language)
                {
                    $productObj->name[$language['id_lang']] = $product['name']['language'];
//                    $productObj->description[$language['id_lang']] = $product['description']['language'];
                    $productObj->link_rewrite[$language['id_lang']] = $product['link_rewrite']['language'];
//                    $productObj->meta_description[$language['id_lang']] = $product['meta_description']['language']['@attributes'];
//                    $productObj->meta_title[$language['id_lang']] = $product['meta_title']['language']['@attributes'];
//                    $productO bj->description_short[$language['id_lang']] = $product['description_short']['language'];
                }
                 $productObj->price = $product['price'];
                //log

                if ($productObj->add()) {
                    $productUpdate = new ProductUpdate();
                    $productUpdate->id_object_remote = $product['id'];
                    $productUpdate->id_object = $productObj->id;
                    $productUpdate->type = 'product';
                    $productUpdate->add();
                }

                //getting and setting the specific price
                $optSP = [
                    'resource' => 'specific_prices',
                    'filter[id_product]' => "1",
                    'display' => '[id]'
                ];

                $specific_price_id = $this->webService->get($optSP);

                $specific_price = (int)json_decode(json_encode($specific_price_id->children()->children()->children()),true);
                $specific_price_detailed = json_decode(json_encode($this->webService->get(['resource' => 'specific_prices','id' => $specific_price])),true);

                $id_specific_price = $this->addSpecificPrice
                (
                    $productObj->id,
                    $specific_price_detailed["specific_price"]["id_product_attribute"],
                    $specific_price_detailed['specific_price']['price'],
                    $specific_price_detailed['specific_price']['reduction'],
                    $specific_price_detailed['specific_price']['reduction_type'],
                    $specific_price_detailed['specific_price']['from'],
                    $specific_price_detailed['specific_price']['to'],
                    $specific_price_detailed['specific_price']['reduction_tax'],
                    $specific_price_detailed['specific_price']['from_quantity']
                );

              
                $productUpdate = new ProductUpdate();
                $productUpdate->id_object_remote = $specific_price;
                $productUpdate->id_object = $id_specific_price;
                $productUpdate->type = 'specific_price';
                $productUpdate->add();

                $optImages = [
                    'resource' => 'images/products',
                    'id' => $product['id']
                ];

                $handle = curl_init("http://".$this->key."@developer.gsvision.eu/1752/api/images/products/".$product['id']);
                curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
                $response = curl_exec($handle);
                $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);

                if( $httpCode == 500) {
                    continue;
                }

                $images = $this->webService->get($optImages);
                $image_ids = [];
                foreach ($images->children()->children() as $image)
                {
                    $image_ids[] = json_decode(json_encode($image->attributes()),true);
                }

                foreach($image_ids as $keyId => $id)
                {
                    if($keyId == 0){
                        $img_cover = true;
                    }else
                    {
                        $img_cover = false;
                    }
                    $urls_image =  "https://".$this->key."@developer.gsvision.eu/1752//".'/api/images/products/'.$product['id'].'/'.$id['@attributes']['id'];
                    $this->img_import($urls_image,$productObj->id,$img_cover);
                }
                $combinations = [];
                if(!array_key_exists('combination',$product['associations']['combinations'])) {
                     dump('This object doesnt have combinations');
                     StockAvailable::setQuantity($productObj->id,0,$product['quantity']);
                     continue;
                }
                foreach ($product['associations']['combinations']['combination'] as $combination) {
                    //Getting combinations
                    $optCombinations = [
                        'resource' => 'combinations',
                        'id' => $combination['id']
                    ];
                    $currentCombination = json_decode(json_encode($this->webService->get($optCombinations)),true);
                    $combinations[] = $currentCombination['combination'];
                }
                foreach ($combinations as $combination) {
                    $finalAttributes = [];
                    foreach ($combination["associations"]["product_option_values"]["product_option_value"] as $option_value) {
                        $option_value_id = $option_value;
                        if(count($combination["associations"]["product_option_values"]["product_option_value"]) > 1) {
                          $option_value_id =  $option_value['id'];
                          $attribute_id = json_decode(json_encode($this->webService->get(['resource' => "product_option_values", "language" => "1", "id" => $option_value_id])), true);
                          $id_attribute = $attribute_id["product_option_value"]["id_attribute_group"];
                          $attribute_group = json_decode(json_encode($this->webService->get(["resource" => "product_options", "language" => "1", "id" => $id_attribute])), true);

                        }else{
                            $attribute_id = json_decode(json_encode($this->webService->get(['resource' => "product_option_values", "language" => "1", "id" => $option_value_id])), true);
                            $id_attribute = $attribute_id["product_option_value"]["id_attribute_group"];
                            $attribute_group = json_decode(json_encode($this->webService->get(["resource" => "product_options", "language" => "1", "id" => $id_attribute])), true);
                        }
                        if($attributeExists = $this->getAttributesByNames($attribute_group['product_option']['name']['language'],$attribute_id['product_option_value']['name']['language'] == 0))
                        {
                            if ($attributeExists['id_group'] == 0) {
                                $attrGroup = new AttributeGroup();
                                $attrGroup->is_color_group = $attribute_group['product_option']["is_color_group"];
                                $attrGroup->position = $attribute_group['product_option']['position'];
                                $attrGroup->group_type = $attribute_group['product_option']['group_type'];
                                foreach ($languages as $language) {
                                    $attrGroup->name[$language['id_lang']] = $attribute_group['product_option']['name']['language'];
                                    $attrGroup->public_name[$language['id_lang']] =  $attribute_group['product_option']['public_name']['language'];
                                }
                                $attrGroup->add();
                                $attributeExists['id_group'] = $attrGroup->id;
                            }
                            if ($attributeExists['id_attribute'] == 0) {
                                $attr = new Attribute();
                                $attr->id_attribute_group =  $attributeExists['id_group'];
                                $attr->position = $attribute_id['product_option_value']['position'];
                                if($attribute_id['product_option_value']['color'] == null){
                                    $attr->color = "Empty";
                                }else {
                                    $attr->color = $attribute_id['product_option_value']['color'];
                                }
                                foreach ($languages as $language){
                                    $attr->name[$language['id_lang']] = $attribute_id["product_option_value"]['name']["language"];
                                }
                                $attr->add();
                                $attributeExists['id_attribute'] = $attr->id;
                            }
                            $finalAttributes[] = $attributeExists['id_attribute'];
                        }
                    }
                    if(!isset($finalAttributes) || empty($finalAttributes)) {
                        continue;
                    }
                    $id_product_attribute = $productObj->addCombinationEntity(
                        0,
                        0,
                        0,
                        0,
                        $combination['quantity'],
                        0,
                        null,
                        $combination['reference'],
                        0,
                        '',
                        ''
                    );
                    $combinationObj = new Combination((int)$id_product_attribute);
                    $combinationObj->id_product = $productObj->id;
                    $combinationObj->setAttributes($finalAttributes);
                    StockAvailable::setQuantity($productObj->id,$id_product_attribute,$combination['quantity']);
                }
            die();
            }

//        }
        return $states[21];
    }

    public function getAccessToken()
    {
        return $this->accessToken;
    }

    public function  getRemoteCategories()
    {
        $this->_init();
        $optCategories = [
            'resource' => 'categories',
        ];
        $remote_categories = array();
        try
        {
            $categories = json_decode(json_encode($this->webService->get($optCategories)),true);
            $remote_categories = array();
            foreach($categories['categories']['category'] as $category)
            {
                $category_ids =  $category['@attributes']['id'];
                $optCategory = [
                    'resource' => 'categories',
                    'id' => $category_ids,
                    'language' => '1',
                ];
                $remoteCategories = json_decode(json_encode($this->webService->get($optCategory)),true);

                foreach ($remoteCategories as $remote)
                {
                    $remote_categories[] = $remote;
                }
            }

            //check if product is in given category

        }
        catch (PrestaShopWebserviceException $ex)
        {
            $trace = $ex->getTrace(); // Retrieve all information on the error
            $errorCode = $trace[0]['args'][0]; // Retrieve the error code
            if ($errorCode == 401)
                echo 'Bad auth key';
            else
                echo 'Other error : <br />'.$ex->getMessage();

        }
        return $remote_categories;
    }

    public function  getLocalCategories()
    {
        $local_categories[] = Category::getCategories($this->context->language->id);
        foreach ($local_categories as  $local_category)
        {
            $local_categories = [];
            foreach ($local_category as $category)
            {
                foreach ($category as $single)
                {

                    $local_categories[] = $single['infos'];
                }
            }
        }
        return $local_categories;
    }


    public function getRemoteManufacturers()
    {
        $this->_init();
        $otpManufacturers = [
            'resource' => 'manufacturers',
        ];
        $remote_manufacturers = $this->webService->get($otpManufacturers);
        $manufacturers = array();
        $remote_single_manufacturer =[];
        foreach($remote_manufacturers->manufacturers->manufacturer as $remote_manufacturer)
        {
           $manufacturers =  json_decode(json_encode($remote_manufacturer->attributes()->id),true);
            foreach($manufacturers as $manufacturer)
            {
                $otpManufacturer = [
                    'resource' => 'manufacturers',
                    'language' => '1',
                    'id'=> $manufacturer,
                ];
                $remote_single_manufacturer[] = json_decode(json_encode($this->webService->get($otpManufacturer)),true);
            }
        }
        return $remote_single_manufacturer;
    }


    public function getAttributesByNames($group,$attribute_name) {
        $attributes = [];
        $query = 'SELECT * FROM `ps_attribute` pa
                LEFT JOIN `ps_attribute_group_lang` pag ON pa.id_attribute_group = pag.id_attribute_group
                LEFT JOIN `ps_attribute_lang` pal ON pal.id_attribute = pa.id_attribute
               ';
        $group =  Db::getInstance()->getRow($query.'WHERE pag.name = "'.$group.'"');
        if (isset($group['id_attribute_group']))
            $attributes['id_group'] = $group['id_attribute_group'];
        else
            $attributes['id_group'] = 0;

        $attribute =  Db::getInstance()->getRow($query.'WHERE pal.name = "'.$attribute_name.'"');
        if (isset($attribute['id_attribute']))
            $attributes['id_attribute'] = $attribute['id_attribute'];
        else
            $attributes['id_attribute'] = 0;


        return $attributes;
    }

   public function addSpecificPrice($id_product, $id_product_attribute = 0, $price = -1, $reduction_value = 0.00, $reduction_type = 'percentage', $from_date = '0000-00-00 00:00:00', $to_date = '0000-00-00 00:00:00', $tax = 1, $from_quantity = 1, $id_shop = 0,   $id_shop_group = 0, $id_currency = 0, $id_country = 0, $id_customer = 0) {
        if (SpecificPrice::exists($id_product, $id_product_attribute, $id_shop , $id_shop_group, $id_country, $id_currency, $id_customer, $from_quantity, $from_date, $to_date, $rule = false))
            return false;
        $specificPrice = new SpecificPrice();
        $specificPrice->id_product = (int)$id_product;
        $specificPrice->id_product_attribute = (int)$id_product_attribute; // if 0 then for all attributes
        $specificPrice->reduction = $reduction_value; // 7% is 0.07
        $specificPrice->reduction_type = $reduction_type;
        $specificPrice->reduction_tax = $tax;
        $specificPrice->id_shop = $id_shop;
        $specificPrice->id_currency = $id_currency;
        $specificPrice->id_country = $id_country;
        $specificPrice->id_group = $id_shop_group;
        $specificPrice->id_customer = $id_customer;
        $specificPrice->price = $price;
        $specificPrice->from_quantity = $from_quantity;
        $specificPrice->from = $from_date;
        $specificPrice->to = $to_date;
        if ($specificPrice->add() &&  $specificPrice->save()) {
            $addedPriceId = $specificPrice->id;
            return $addedPriceId;
        } else {
            return false;
        }
    }



    private function img_import($url, $id_product, $cover = false)
    {
        if (!empty($url)) {
            $shops = Shop::getContextListShopID();
            $url = str_replace(' ', '%20', $url);
            $image = new Image();
            $image->id_product = (int) $id_product;
            $image->position = Image::getHighestPosition($id_product) + 1;
            $image->cover = ($cover) ? true : false;
            $image->add();
            $image->associateTo($shops);
            if (!$this->copyImg($id_product, $image->id, $url, 'products', true)) {

                $image->delete();
            }
        }
    }

    private function copyImg($id_entity, $id_image, $url, $entity = 'products', $regenerate = true)
    {
        $tmpfile = tempnam(_PS_TMP_IMG_DIR_, 'ps_import');
        $watermark_types = explode(',', Configuration::get('WATERMARK_TYPES'));
        switch ($entity) {
            default:
            case 'products':
                $image_obj = new Image($id_image);
                $path = $image_obj->getPathForCreation();
                break;
            case 'categories':
                $path = _PS_CAT_IMG_DIR_ . (int)$id_entity;
                break;
            case 'manufacturers':
                $path = _PS_MANU_IMG_DIR_ . (int)$id_entity;
                break;
            case 'suppliers':
                $path = _PS_SUPP_IMG_DIR_ . (int)$id_entity;
                break;
            case 'stores':
                $path = _PS_STORE_IMG_DIR_ . (int)$id_entity;
                break;
        }
        //FIX FOR SSL ISSUE ON LOCAL
        $context = stream_context_set_default(array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
            )
        ));
        $response = file_get_contents($url, false,$context);
        if (!$response)
            return false;
        if (file_put_contents($tmpfile, $response)) {

            if (!ImageManager::checkImageMemoryLimit($tmpfile)) {
                @unlink($tmpfile);
                return false;
            }
            $tgt_width = $tgt_height = 0;
            $src_width = $src_height = 0;
            $error = 0;
            ImageManager::resize($tmpfile, $path . '.jpg', null, null, 'jpg', false, $error, $tgt_width, $tgt_height, 5, $src_width, $src_height);
            $images_types = ImageType::getImagesTypes($entity, true);
            if ($regenerate) {
                $previous_path = null;
                $path_infos = array();
                $path_infos[] = array($tgt_width, $tgt_height, $path . '.jpg');
                foreach ($images_types as $image_type) {
                    $tmpfile = $this->get_best_path($image_type['width'], $image_type['height'], $path_infos);
                    if (ImageManager::resize(
                        $tmpfile,
                        $path . '-' . stripslashes($image_type['name']) . '.jpg',
                        $image_type['width'],
                        $image_type['height'],
                        'jpg',
                        false,
                        $error,
                        $tgt_width,
                        $tgt_height,
                        5,
                        $src_width,
                        $src_height
                    )) {
                        if ($tgt_width <= $src_width && $tgt_height <= $src_height) {
                            $path_infos[] = array($tgt_width, $tgt_height, $path . '-' . stripslashes($image_type['name']) . '.jpg');
                        }
                        if ($entity == 'products') {
                            if (is_file(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int)$id_entity . '.jpg')) {
                                unlink(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int)$id_entity . '.jpg');
                            }
                            if (is_file(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int)$id_entity . '_' . (int)Context::getContext()->shop->id . '.jpg')) {
                                unlink(_PS_TMP_IMG_DIR_ . 'product_mini_' . (int)$id_entity . '_' . (int)Context::getContext()->shop->id . '.jpg');
                            }
                        }

                    }
                   Hook::exec('actionWatermark', array('id_image' => $id_image, 'id_product' => $id_entity));
                }
            }
        } else {
            @unlink($tmpfile);
            return false;
        }

        return true;
    }
    public function get_best_path($tgt_width, $tgt_height, $path_infos)
    {
        $path_infos = array_reverse($path_infos);
        $path = '';
        foreach ($path_infos as $path_info) {
            list($width, $height, $path) = $path_info;
            if ($width >= $tgt_width && $height >= $tgt_height) {
                return $path;
            }
        }
        return $path;
    }


    public function enable($force_all = false)
    {
        return parent::enable($force_all)
            && $this->installTab()
            ;
    }

    public function disable($force_all = false)
    {
        return parent::disable($force_all)
            && $this->uninstallTab()
            ;
    }

    private function installTab()
    {
        $tabId = (int) Tab::getIdFromClassName('AdminMapCategories');
        if (!$tabId) {
            $tabId = null;
        }
        $tab = new Tab($tabId);
        $tab->active = 1;
        $tab->class_name = 'AdminMapCategories';
        // Only since 1.7.7, you can define a route name
        $tab->route_name = 'admin_my_symfony_routing';
        $tab->name = array();
        foreach (Language::getLanguages() as $lang) {
            $tab->name[$lang['id_lang']] = $this->
            trans('Map Categories', array(), 'Modules.gswsproductsupdate.Admin', $lang['locale']);
        }
        $tab->id_parent = (int) Tab::getIdFromClassName('Improve');
        $tab->module = $this->name;

        return $tab->save();
    }

    private function uninstallTab()
    {
        $tabId = (int) Tab::getIdFromClassName('AdminMapCategories');
        if (!$tabId) {
            return true;
        }

        $tab = new Tab($tabId);

        return $tab->delete();
    }
}
